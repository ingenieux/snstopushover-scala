package br.com.ingenieux.snstopushover

import org.scalatest.FunSuite
import io.Source._
import scala.util.parsing.json.JSON

class HandlerSpec extends FunSuite {
  test("A Handler must be called") {
    val content = fromInputStream(getClass.getResourceAsStream("/request.json")).mkString

    new Handler(content, "XPl84gBgqiKe4KbMKk9GFnUHrXgM0Z", None)
  }
}
