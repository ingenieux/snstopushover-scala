package br.com.ingenieux.snstopushover

import com.twitter.finatra._
import com.twitter.finatra.ContentType._
import akka.actor._

object Server extends FinatraServer {
  val service = new Service()

  implicit val system = ActorSystem("sns-to-pushover")

  val messenger = system.actorOf(Props[Messenger], "messenger")

  register(service)
}