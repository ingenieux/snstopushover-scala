package br.com.ingenieux.snstopushover

import com.twitter.finatra.Controller
import java.nio.charset.Charset
import scala.util.parsing.json.JSON

class Service extends Controller {
  get("/") { req =>
    render.plain("Hello, World!").toFuture
  }

  post("/sns/:userId/:deviceName") { req =>
    val body = req.content.toString(Charset.forName("UTF-8"))

    val userId: String = req.routeParams.get("userId").get
    val deviceName: Option[String] = req.routeParams.get("deviceName")

    new Handler(body, userId, deviceName)

    render.plain("OK").toFuture
  }
}

