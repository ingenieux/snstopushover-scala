package br.com.ingenieux.snstopushover

import akka.actor._
import java.net.URL
import scala.io.Source

object Messages {
  case class AWSConfirm(confirmationUrl: String)

  case class ValidateOnPushover(userId: String, deviceName: Option[String])

  case class SendToPushover(userId: String, deviceName: Option[String], title: String, message: String)
}
