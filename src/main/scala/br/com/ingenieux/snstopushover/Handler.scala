package br.com.ingenieux.snstopushover

import akka.actor._
import com.codahale.jerkson.Json._

class Handler(body: String, userId: String, deviceName: Option[String], messenger: ActorRef = Server.messenger) {
  val args = parse[Map[String, _]](body)
  val op = args.get("Type").get.asInstanceOf[String]

  op match {
    case "SubscriptionConfirmation" => snsConfirmSubscription()
    case "Notification" => snsNotification()
  }

  def snsConfirmSubscription() {
    messenger ! Messages.ValidateOnPushover(userId, deviceName)

    val subscribeUrl = args.get("SubscribeURL").get.asInstanceOf[String]

    messenger ! Messages.AWSConfirm(subscribeUrl)
  }

  def snsNotification() {
    val subject = args.get("Subject").get.asInstanceOf[String]
    val message = args.get("Message").get.asInstanceOf[String]

    messenger ! Messages.SendToPushover(userId, deviceName, subject, message)
  }
}
