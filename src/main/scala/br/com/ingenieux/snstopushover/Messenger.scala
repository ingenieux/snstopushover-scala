package br.com.ingenieux.snstopushover

import akka.actor.Actor
import scala.io.Source
import java.net.URL
import dispatch._
import scala.concurrent.ExecutionContext.Implicits.global

class Messenger extends Actor {
  val tokenId = "gSpcGV4gIbuRyrRldYgxTLvIjOQSmM"

  def receive = {
    case Messages.AWSConfirm(confirmationUrl) =>
      val output = Source.fromInputStream(new URL(confirmationUrl).openStream()).mkString
      println(s"Output: $output")
    case Messages.ValidateOnPushover(userId, deviceName) =>
      val postFields = List(("user" -> userId), ("token" -> tokenId))

      if (deviceName.isDefined) {
        postFields :+ ("device" -> deviceName.get)
      }

      val req = url("https://api.pushover.net/1/users/validate.json").POST << postFields

      val h = new Http

      val result = h(req OK as.String)

      result() match {
        case x: String => println(s"Ok: $x")
      }

    case Messages.SendToPushover(userId, deviceName, title, message) =>
      val postFields = List(("user" -> userId), ("token" -> tokenId), ("title" -> title), ("message" -> message))

      if (deviceName.isDefined) {
        postFields :+ ("device" -> deviceName.get)
      }

      val req = url("https://api.pushover.net/1/messages.json").POST << postFields

      val h = new Http

      val result = h(req OK as.String)

      result() match {
        case x: String => println(s"Ok: $x")
      }

      println("Sent To Pushover")
  }
}
