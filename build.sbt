name := "snstopushover"

organizationName := "br.com.ingenieux"

version := "0.0.1-SNAPSHOT"

scalaVersion := "2.10.3"

resolvers ++= Seq("snapshots"     at "http://oss.sonatype.org/content/repositories/snapshots",
                  "staging"       at "http://oss.sonatype.org/content/repositories/staging",
                  "releases"      at "http://oss.sonatype.org/content/repositories/releases",
                  "twttr"         at "http://maven.twttr.com/"
                 )

scalacOptions ++= Seq("-deprecation", "-unchecked", "-explaintypes")

seq(com.typesafe.sbt.SbtNativePackager.packagerSettings: _*)

packageArchetype.java_application

libraryDependencies ++= {
  Seq(
      "commons-lang"               % "commons-lang"     % "2.6",
      "com.amazonaws"              % "aws-java-sdk"     % "1.6.5",
      "com.twitter"                % "finatra"          % "1.4.1",
      "com.typesafe.akka"         %% "akka-actor"       % "2.2.3",
      "com.cloudphysics"           % "jerkson_2.10"     % "0.6.3",
      "net.databinder.dispatch"   %% "dispatch-core"    % "0.11.0",
      "org.scalatest"              % "scalatest_2.10.0" % "2.0.M5" % "test"
  )
}

mainClass in (Compile, run) := Some("br.com.ingenieux.snstopushover.Server")          